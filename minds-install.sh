﻿#!/bin/sh
echo "YOU MUST BE ROOT USER, ARE YOU ROOT!?? This script will fail otherwise" 

echo "Downloading minds/upstream source code"
git clone https://gitlab.com/minds/minds.git
cd minds

echo "Getting dependencies"
apt-get update
apt-get install git -y
apt install docker.io -y
apt-get install docker-compose -y

echo "Setting up elasticsearch"
sysctl -w vm.max_map_count=262144
docker-compose down
docker-compose up elasticsearch-legacy-provisioner
docker-compose up elasticsearch-provisioner

echo "installing the front and engine repositories"
sh init.sh

echo "Deploying Minds Docker Build, This could take a while..."
docker-compose up -d nginx
docker-compose up installer
docker-compose up front-build
docker-compose up sync-engine
docker-compose up sync-front
echo " Install Complete, Open http://localhost:8080/login"
firefox http://localhost:8080/login/